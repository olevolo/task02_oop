/**
 * @author Volodymyr Oleksiuk
 * @subject individual task #1 "chief cook"
 * @version 1.0  19 Aug 2017
 * 
 * used:
 * OOP principles
 * UML diagram
 * log4j API
 * exceptions
 * IO with files
 * annotaions
 * reflection
 */

import java.io.*;
import org.apache.log4j.Logger;
import view.*;
import view.Menu;

public class Main {

	public static void main(String[] args) throws IOException {

		Menu.show();
		Menu.input();
	}


}
