package vegetables;

import java.util.Comparator;

public class VegetableCaloriesComparator  implements Comparator<Vegetable> {

		public int compare(Vegetable arg0, Vegetable arg1) {
			
			return  (arg0.getTotalCalories() > arg1.getTotalCalories())
					? 1 
				    : (arg0.getTotalCalories() < arg1.getTotalCalories())
				      ? -1 
					  : 0;
		}
}
