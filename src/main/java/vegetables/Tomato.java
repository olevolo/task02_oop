package vegetables;

@SuppressWarnings("serial")
public class Tomato extends Vegetable {
	private static final int TOMATO_CALORIES_PER_100G = 15;

	public Tomato(double weight) {
		this.weight = weight;
		this.totalCalories = calcCalories();
	}
	

	@Override
	protected double calcCalories() {
		/*
		 *    100 g -  45 ccal
		 * weight g -  ?  ccal
		 */
		return weight * TOMATO_CALORIES_PER_100G * 0.01;
	}


	@Override
	public String toString() {
		return "Tomato [weight=" + weight + ", totalCalories=" + (int)totalCalories + "]";
	}

	
}
