package vegetables;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class Vegetable implements Serializable {
	
	protected double weight;
	protected double totalCalories;

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
		this.totalCalories = calcCalories();
	}

	
	public double getTotalCalories() {
		return totalCalories;
	}

	abstract protected double calcCalories();
	
	
}
