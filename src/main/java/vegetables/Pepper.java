package vegetables;

@SuppressWarnings("serial")
public class Pepper extends Vegetable {
	private static final int PEPPER_CALORIES_PER_100G = 19;

	public Pepper(double weight) {
		this.weight = weight;
		this.totalCalories = calcCalories();
	}
	

	@Override
	protected double calcCalories() {
		return weight * PEPPER_CALORIES_PER_100G * 0.01;
	}


	@Override
	public String toString() {
		return "Pepper [weight=" + weight + ", totalCalories=" + (int)totalCalories + "]";
	}
}
