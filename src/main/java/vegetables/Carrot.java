package vegetables;

@SuppressWarnings("serial")
public class Carrot extends Vegetable {

	private static final int CARROT_CALORIES_PER_100G = 45;

	public Carrot(double weight) {
		this.weight = weight;
		this.totalCalories = calcCalories();
	}
	

	@Override
	protected double calcCalories() {
		return weight * CARROT_CALORIES_PER_100G * 0.01;
	}


	@Override
	public String toString() {
		return "Carrot [weight=" + weight + ", totalCalories=" + (int)totalCalories + "]";
	}

}
