package vegetables;

import vegetables.Vegetable;

@SuppressWarnings("serial")
public class Cabbage extends Vegetable {

	private static final int CABBAGE_CALORIES_PER_100G = 23;

	public Cabbage(double weight) {
		this.weight = weight;
		this.totalCalories = calcCalories();
	}
	

	@Override
	protected double calcCalories() {
		return weight * CABBAGE_CALORIES_PER_100G * 0.01;
	}


	@Override
	public String toString() {
		return "Cabbage [weight=" + weight + ", totalCalories=" + (int)totalCalories + "]";
	}

}
