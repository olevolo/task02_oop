package vegetables;

@SuppressWarnings("serial")
public class Cucumber extends Vegetable {

	private static final int CUCUMBER_CALORIES_PER_100G = 15;

	public Cucumber(double weight) {
		this.weight = weight;
		this.totalCalories = calcCalories();
	}
	

	@Override
	protected double calcCalories() {
		return weight * CUCUMBER_CALORIES_PER_100G * 0.01;
	}


	@Override
	public String toString() {
		return "Cucumber [weight=" + weight + ", totalCalories=" + (int)totalCalories + "]";
	}

}
