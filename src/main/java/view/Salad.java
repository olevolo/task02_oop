package view;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;
import vegetables.*;

public class Salad {

	private static final Logger innerLogger = Logger.getLogger(Salad.class);
	
	private List<Vegetable> ingredients;
	private double caloriesSum;
	
	public Salad() {
		ingredients = new ArrayList<>();
		caloriesSum = 0;
	}
	
	//reading data from file
	public Salad(BufferedReader reader) throws FileNotFoundException, IOException {
		ingredients = new ArrayList<Vegetable>();
		caloriesSum = 0;
		try {
			String line = null, identifier;
			String[] components;
			while ((line = reader.readLine()) != null) {						//going through the file
		        components = line.split(" ");
		        identifier = components[0];
		        switch (identifier) {
		        	case "tomato":
		        		ingredients.add(new Tomato(Double.parseDouble(components[1])));
		        		break;
		        	case "cucumber":
		        		ingredients.add(new Cucumber(Double.parseDouble(components[1])));
		        		break;
		        	case "cabbage":
		        		ingredients.add(new Cabbage(Double.parseDouble(components[1])));
		        		break;
		        	case "pepper":
		        		ingredients.add(new Pepper(Double.parseDouble(components[1])));
		        		break;
		        	case "carrot":
		        		ingredients.add(new Carrot(Double.parseDouble(components[1])));
		        		break;
		        	default:
		        		throw new IllegalArgumentException("Invalid vegetable name: " + identifier);
		        }
		    }
		} catch (IllegalArgumentException y) {
			innerLogger.error(y);
		} catch (IOException x) {
		    innerLogger.error("IOException: %s%n", x);
		}
		caloriesSum = sum();
		
		//writing initialization parameters to file parameters.txt
		ObjectOutputStream dataOutputStream = new ObjectOutputStream(new DataOutputStream(new FileOutputStream(new File("parameters.txt"))));
		for (Vegetable v : ingredients)
			dataOutputStream.writeObject(v);
		dataOutputStream.close();
	}
	
	private double sum() {
		for (int i = 0; i < ingredients.size(); i++)
			caloriesSum += ingredients.get(i).getTotalCalories();
		return caloriesSum;
	}
	
	//sorting vegetables from smaller to bigger 
	public void sortSalad() {
		Comparator<Vegetable> comp = new VegetableCaloriesComparator(); 
		ingredients.sort(comp);
	}
	
	//find vegetables which calories number are in [a, b] interval
	public void find(double a, double b) {
		System.out.println("Searching...\nFound in [" + a + ", " + b + "] interval:");
		
		for (Vegetable v : ingredients) 
			if ((v.getTotalCalories() >= a) && (v.getTotalCalories() <= b))
				System.out.println(v);
	}
	
	public void print() {
		System.out.println("\nview.Salad: " + this.getClass().getSimpleName());
		System.out.println("Total calories: " + (int)caloriesSum);
		System.out.println("\nIngredients:");
		for (Vegetable v : ingredients)
			System.out.println(v);
		System.out.println();
	}
}
