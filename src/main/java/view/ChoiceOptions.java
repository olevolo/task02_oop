package view;

public enum ChoiceOptions {
	FIRST("Make a salad"),
	SECOND("Sort vegetables in the salad"),
	THIRD("Find vegetables in the salad with certain calories number"),
	FOURTH("Exit");
	
	private String option;
	
	ChoiceOptions(String option) {
		this.option = option;
	}
	
	public String toString() {
		return option;
	}
}
