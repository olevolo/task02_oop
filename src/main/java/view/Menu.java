package view;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Menu {

	private static final Logger logger = Logger.getLogger(Menu.class);

	private static ChoiceOptions[] options = ChoiceOptions.values();
	private static Salad salad = new Salad();
	
	public Menu() { }
	
	public static void show() {
		System.out.println("Choose an option:");
		for(ChoiceOptions cho : options)
            System.out.printf("%d) %s%n", cho.ordinal() + 1, cho.toString());
	}

	//input from user
	public static void input() throws IOException {
		String choice;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		choice = br.readLine();
		switch (choice) {
			case "1":
				FileReader fr = new FileReader("shop.txt");
				BufferedReader fbr = new BufferedReader(fr);
				salad = new Salad(fbr);
				salad.print();
				System.out.println("What's next:");
				show();
				input();
				break;
			case "2":
				salad.sortSalad();
				salad.print();
				System.out.println("What's next:");
				show();
				input();
				break;
			case "3":
				salad.find(0, 12);
				System.out.println("\nWhat's next:");
				show();
				input();
				break;
			case "4":
				logger.info("Done.");
				System.exit(0);
			default:
				logger.error("Invalid info!");
				System.out.println("Try again:");
				input();
		}
		br.close();
		logger.info("Done.");
	}
}
